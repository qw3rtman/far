#include <stdio.h>
#include <string.h>
#include <stdlib.h>

char *find_and_replace_single(const char *s, const char *old, const char *new)
{
    char *output;
    int i = 0;
		int count = 0;

    for (i = 0; s[i] != '\0'; i++) {
      if (strstr(&s[i], old) == &s[i]) {
        count++;

        i += strlen(old) - 1;
      }
    }

    output = (char *) malloc(i + count * (strlen(new) - strlen(old)));

    if (output == NULL) {
			exit(EXIT_FAILURE);
		}

    i = 0;

    while (*s) {
      if (strstr(s, old) == s) { //compare the substring with the newstring
        strcpy(&output[i], new);

        i += strlen(new); //adding newlength to the new string
        s += strlen(old); //adding the same old length the old string
      } else {
				output[i++] = *s++;
			}
    }

    output[i] = '\0';

    return output;
}

char *find_and_replace_multiple()
{
  return "t";
}

int main(int argc, char const *argv[]) {
	puts(find_and_replace_single("This is a test.", "This", "That"));

  puts(find_and_replace_multiple());

	return 0;
}
