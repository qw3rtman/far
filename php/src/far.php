<?php

/**
 * Not going to worry about types at all.
 * Will be written in C at the end anyway, so it doesn't really matter here.
 */

function far($input, $find, $replace)
{
	$input_split = str_split($input, 1);

	for ($i = 0; $i < count($input_split); $i++) {
		if (substr($input, $i, strlen($find)) == $find) {
			$input_split[$i] = $replace;

			for ($j = $i + 1; $j < $i + strlen($find); $j++) {
				$input_split[$j] = "";
			}
		}
	}

	return implode($input_split);
}

echo far("ababab", "ab", "b") . "\n"; // Expected: bbb

echo far("ababab", "a", "aa") . "\n"; // Expected: aabaabaab

echo far("ababab", "ba", "aa") . "\n"; // Expected: aaaaab

echo far("ababab", "ab", "baa") . "\n"; // Expected: baabaabaa
